# TP2-POO2-UML

## Aclaraciones:

- El diagrama lo realicé en https://app.diagrams.net/

- Usé un constructor para cada clase, exceptuando "Empleado" ya que la misma no se podrá instanciar directamente a excepción de cada empleado específico(de planta permanente, de planta temporaria, o contratado), a pesar de que usando Wollok, el constructor no es necesario.

- Asumo que existen los tipos de datos "PaymentReceipt" ( como recibo de sueldo ) y "PaymentMethod" ( como medio de pago ). [ Incluidos como clases que se instancian al momento de consultarse en el recibo de haber y en la instanciación del empleado contratado (Solución hecha previamente en Wollok) ].

- Aquellos métodos privados son los auxiliares con los cuales no se necesita operar específicamente.

